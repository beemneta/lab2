///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file short.c
/// @version 1.0
///
/// Print the characteristics of the "short", "signed short" and "unsigned short" datatypes.
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   18_Jan_2020
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "long.h"


///////////////////////////////////////////////////////////////////////////////
/// short

/// Print the characteristics of the "short" datatype
void doLong() {
   printf(TABLE_FORMAT_LONG, "long",8*CHAR_BIT ,8*CHAR_BIT/8,LONG_MIN, LONG_MAX);
}


/// Print the overflow/underflow characteristics of the "short" datatype
void flowLong() {
   long overflow = LONG_MAX;
   printf("long overflow: %ld + 1 ",overflow++);
   printf("becomes %ld\n", overflow);

   long underflow = LONG_MIN;
   printf("long underflow: %ld - 1 ", underflow--);
   printf("becomes %ld\n", underflow);
}


///////////////////////////////////////////////////////////////////////////////
/// unsigned short

/// Print the characteristics of the "unsigned short" datatype
void doUnsignedLong() {
   printf(TABLE_FORMAT_ULONG,"unsigned long",8*CHAR_BIT,8*CHAR_BIT/8, 0, ULONG_MAX);
}

/// Print the overflow/underflow characteristics of the "unsigned short" datatype
void flowUnsignedLong() {

   unsigned long overflow = ULONG_MAX;
   printf("unsigned long overflow: %lu + 1 ",overflow++);
   printf("becomes %lu\n", overflow);


   unsigned long underflow = 0;
   printf("unsigned long underflow: %lu - 1 ", underflow--);
   printf("becomes %lu\n", underflow);
}

