///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.h
/// @version 1.0
///
/// Print the characteristics of the "int" and "unsigned int" datatypes.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

extern void doLong();            /// Print the characteristics of the "short" datatype
extern void flowLong();          /// Print the overflow/underflow characteristics of the "short" datatype

/// Print the characteristics of the "signed short" datatype
    /// Print the overflow/underflow characteristics of the "signed short" datatype

extern void doUnsignedLong();    /// Print the characteristics of the "unsigned short" datatype
extern void flowUnsignedLong();  /// Print the overflow/underflow characteristics of the "unsigned short" datatype

